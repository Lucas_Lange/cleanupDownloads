from os import listdir, rename
from os.path import join, isdir, isfile
from shutil import move
from pathlib import Path
from zipfile import ZipFile

# This file is for cleaning up the Downloads folder -> it creates folders for different file types and moves them into the right folder

# Functionality to add:
# TODO -> add unzip functionality
#           Only unzip if file is not already in unzipped folder and was moved to zipped folder -> either via bash movement checker or via python func that is called after moving
#     -> add .sh file to run this script automatically every time something is downloaded
#     -> what happens if a file is already in the folder -> check if file is already in folder and if so -> rename it
#           Error:
#           move(src_path, dst_path)
#           File "/usr/lib/python3.10/shutil.py", line 814, in move
#           raise Error("Destination path '%s' already exists" % real_dst)
#           shutil.Error: Destination path '/home/lucas/Downloads/Docs/jjmmtt_antrag_vorlage.dotx' already exists
#           -> add try except block and maybe ask user or rename file
#     -> dont rename Programs to lowercase
#     -> add function file
#     -> add print that lets you know how many files were moved and to which location -> example X files were moved to Images


data_type_dict = {
    "Images": ['.jpg', '.png', '.jpeg', '.heic'],
    "Movies": ['.mp4', '.mov', '.avi', '.mkv'],
    "PDFs": ['.pdf'],
    "Zips": ['.zip', '.gz', '.tar', '.tar.zst'],
    "Docs": ['.docx', '.doc', '.odt', '.txt', '.rtf', '.pptx', '.ppt', '.dotx'],
    "Data": ['.csv', '.xlsx', '.xls'],
    "Programs": ['.deb', '.sh', '.py']
}


def setup():
    folders_to_create: list[str] = ['PDFs', 'Images', 'Zips', 'Docs', 'Other', 'Data', 'Programs', 'Folders', 'Unzipped', 'Movies']
    downloads_path: str = join(Path.home(), 'Downloads')

    for folders in folders_to_create:
        if folders not in listdir(downloads_path):
            Path(join(downloads_path, folders)).mkdir(parents=True, exist_ok=True)

    # make all files lowercase and replace spaces with underscores
    for file in files_in_download_folder(downloads_path, folders_to_create):
        if " " in file:
            rename(join(downloads_path, file), join(downloads_path, file.lower().replace(" ", "_")))
        else:
            rename(join(downloads_path, file), join(downloads_path, file.lower()))

    return downloads_path, files_in_download_folder(downloads_path, folders_to_create)


def files_in_download_folder(downloads_path, created_folders):
    return [x for x in listdir(downloads_path) if x not in created_folders]


def move_files(ls_files_without_created_folders):
    for current_file in ls_files_without_created_folders:
        src_path = join(download_path, current_file)
        if isdir(src_path) and not isfile(src_path):
            dst_path = join(download_path, 'Folders')
        else:
            if any(current_file.endswith(x) for x in data_type_dict['PDFs']):
                dst_path = join(download_path, 'PDFs')
            elif any(current_file.endswith(x) for x in data_type_dict['Movies']):
                dst_path = join(download_path, 'Movies')
            elif any(current_file.endswith(x) for x in data_type_dict['Images']):
                dst_path = join(download_path, 'Images')
            elif any(current_file.endswith(x) for x in data_type_dict['Zips']):
                dst_path = join(download_path, 'Zips')
            elif any(current_file.endswith(x) for x in data_type_dict['Docs']):
                dst_path = join(download_path, 'Docs')
            elif any(current_file.endswith(x) for x in data_type_dict['Data']):
                dst_path = join(download_path, 'Data')
            elif any(current_file.endswith(x) for x in data_type_dict['Programs']):
                dst_path = join(download_path, 'Programs')
            else:
                dst_path = join(download_path, 'Other')
            # print(current_file, "->", needed_path)
        move(src_path, dst_path)


# WIP
def unzip(file):
    zipped_file = ZipFile(file=join(join(download_path, 'Zips'), file))
    zipped_file.extractall(path=join(join(download_path, 'Unzipped'), file))


if __name__ == '__main__':
    download_path, files_in_download_folder = setup()
    move_files(files_in_download_folder)
